<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>Document</title>
    </head>

    <body>

        <div class="overflow-x-auto">
            <div class="min-w-screen min-h-screen bg-gray-100 flex items-center justify-center bg-gray-100 font-sans overflow-hidden">
                <div class="w-full lg:w-5/6">
                    <div class="flex space justify-between" >
                        <h1 class="font-bold text-3xl" >CRUD LARAVEL</h1>
                        <button id="button-show-modal" class="py-3 px-6 text-white rounded-lg bg-green-400 shadow-lg block md:inline-block">NUEVO</button>
                    </div>
                    
                    <div class="bg-white shadow-md rounded my-6">
                        <table class="min-w-max w-full table-auto">
                            <thead>
                                <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                    <th class="py-3 px-6 text-left">id</th>
                                    <th class="py-3 px-6 text-left">nombre</th>
                                    <th class="py-3 px-6 text-center">lote</th>
                                    <th class="py-3 px-6 text-center">fecha vencimiento</th>
                                    <th class="py-3 px-6 text-center">marca</th>
                                    <th class="py-3 px-6 text-center">modelo</th>
                                    <th class="py-3 px-6 text-center">actions</th>
                                </tr>
                            </thead>

                            <tbody class="text-gray-600 text-sm font-light" id="table-body">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('templates/modals')

        <script>
            const CSRF_TOKEN = "{{csrf_token()}}" 
        </script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
        <script type="module" src="{{asset('js/app.js')}}"></script>

    </body>
</html>

