<?php

namespace App\Http\Controllers;
use App\Models\ProductModel;
use Illuminate\Http\Request;

class ProductController extends Controller

{
    public function index(){
        return view("products");
    }
    
    public function get(){
        $products = ProductModel::get();
        return response()->json($products);
    }

    public function create(Request $request){
        
        $body =  $request->getContent();
        $body = json_decode($body,true);
        unset($body["id"]);
        $res = ProductModel::insert($body);
        $lastProduct = ProductModel::latest('id')->first();

        if(!$res) return $this->sendJson($this->messages("errors","PRODUCT_NOT_REGISTERED"),500); 
        
        $message = $this->messages("success","PRODUCT_REGISTERED");
        $message["data"] = $lastProduct;
        
        return $this->sendJson($message,200);        
    }
    
    public function update(Request $request){
        $content =  $request->getContent();
        $data = json_decode($content,true);
        $id = $data["id"];
        unset($data["id"]);
        $result = ProductModel::where('id',$id)->update($data);
        // $update = ProductModel::where('id',$id)->first();
        
        // if(!$result) return $this->sendJson($this->messages("errors","PRODUCT_NOT_UPDATE"),500); 
        
        $message = $this->messages("success","PRODUCT_UPDATED");
        // $message["data"] = $update;
        
        return $this->sendJson($this->messages("success","PRODUCT_UPDATED"),200); 
    }

    public function delete(Request $request){
        $content =  $request->getContent();
        $data = json_decode($content,true);
        $id = $data["id"];
        $result = ProductModel::where('id',$id)->delete($data);
        
        if(!$result) return $this->sendJson($this->messages("errors","PRODUCT_NOT_DELETE"),500); 
        
        $message = $this->messages("success","PRODUCT_DELETE");
        $message["data"] = $result;
        
        return $this->sendJson($this->messages("success","PRODUCT_DELETE"),200);
    }


    public function messages($type,$name){
        $messages = [
            
            "success" => [
                "PRODUCT_REGISTERED" => ["http_code" => 200,"message" => "producto registrado", "data" => ""],
                "PRODUCT_UPDATED"    => ["http_code" => 200,"message" => "producto actualizado", "data" => ""],
                "PRODUCT_DELETE"     => ["http_code" => 200,"message" => "producto eliminado", "data" => ""]
            ],
            
            "errors" => [
                "PRODUCT_NOT_REGISTERED" => ["http_code" => 500,"message" => "el producto no se registro"],
                "PRODUCT_NOT_UPDATE"     => ["http_code" => 500,"message" => "el producto no se actualizo"],
                "PRODUCT_NOT_DELETE"     => ["http_code" => 500,"message" => "el producto no se elimino"]
                
            ]
        ];

        return $messages[$type][$name];
    }


    public function sendJson($json,$code){
        return response($json,$code)->header('Content-Type', 'application/json');
    }
}
