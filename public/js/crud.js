
class Crud{
   
    constructor(){
        
        // Modal forms

        this.formUpdateProduct = document.getElementById("update-form");
        this.formDeleteProduct = document.getElementById("delete-form");
        this.formAddNewProduct = document.getElementById("new-form");
        
        this.formUpdateProduct.addEventListener("submit", async (e) => await this.submitUpdate(e));
        this.formDeleteProduct.addEventListener("submit", async (e) => await this.submitDelete(e));
        this.formAddNewProduct.addEventListener("submit", async (e) => await this.submitNew(e));

        // table body
        this.tbody = document.getElementById("table-body");


        this.idProduct = 0;
        this.products  = [];
        this.actualproduct = {};

        this.product = {
            nombre: null,
            lote  : null,
            vencimiento : null,
            marca  : null,
            modelo : null
        };

        // Load modal
        this.LoadedupdateProductModal();
        this.LoadedDeleteProductModal();
        this.LoadednewProductModal();

        this.getData();

    }

    async getData(){
        let res = await fetch("http://localhost:8000/products/get",{method: "GET"});

        if (res.status == 200) {
            let products = await res.json();
            
            if(products.length == 0){
                this.tbody.innerHTML = ` <tr id="no-data" class="p-4"> <td colspan="7" class="flex justify-center p-4" > No hay Productos registrados</td> </tr>`
                return;
            }
            
            this.products = products;
            let fragment  = document.createDocumentFragment()
            this.products.map( product => fragment.appendChild(this.productRow(product)));  
            this.tbody.appendChild(fragment)
        }
    }

    productRow(product){
        let tr = document.createElement("tr");
        tr.classList.add("border-b","border-gray-200","hover:bg-gray-100");
        tr.innerHTML = this.templateRow(product);
        tr.appendChild(this.actionsProduct(product,tr));
        return tr;
    }

    actionsProduct({id},tr){

        let containerActionProducts = document.createElement("td");
        let divActions = document.createElement("div");

        containerActionProducts.classList.add("py-3","px-6","text-center")
        divActions.classList.add("flex","item-center","justify-center");

        let updateProduct = document.createElement("div");  
        let deleteProduct = document.createElement("div");

        let elementClass = ["w-4", "mr-2", "transform", "hover:text-purple-500", "hover:scale-110"]; 

        updateProduct.classList.add(...elementClass);
        deleteProduct.classList.add(...elementClass);

        updateProduct.innerHTML = this.iconUpdate();
        deleteProduct.innerHTML = this.iconDelete();
        
        updateProduct.addEventListener("click",()=>{
            
            Crud.activeModal(this.containerUpdateProduct,this.updateModal);
            this.loadActualProduct(id);
            console.log(this.actualproduct)
            this.updateData(this.actualproduct,tr);
            
        })

        deleteProduct.addEventListener("click",()=>{
            
            Crud.activeModal(this.containerDeleteProduct,this.deleteModal);
            this.loadActualProduct(id);
            this.deleteData(this.actualproduct,tr);

        })

        divActions.appendChild(updateProduct);
        divActions.appendChild(deleteProduct);

        containerActionProducts.appendChild(divActions);

        return containerActionProducts;
    }

    loadActualProduct(id){
        for (const product of this.products) {
            if(product.id == id) {
                this.actualproduct = this.products[this.products.indexOf(product)];
                break;
            }
        }
    }

    templateRow({id,nombre,lote,vencimiento,marca,modelo}){
    
        return `
            <td class="py-3 px-6 text-left whitespace-nowrap">
                <div class="flex items-center">
                    <span class="font-medium">${id}</span>
                </div>
            </td>

            <td class="py-3 px-6 text-left">
                <div class="flex items-center">
                    <span>${nombre}</span>
                </div>
            </td>
            
            <td class="py-3 px-6 text-left">
                <div class="flex items-center">
                    <p>${lote}</p>
                </div>
            </td>
            
            <td class="py-3 px-6 text-center">
                <div>
                    <span class="bg-red-200 text-red-600 py-1 px-3 font-bold rounded-full text-xs">${vencimiento}</span>
                </div>
            </td>

            <td class="py-3 px-6 text-left">
                <div class="flex items-center">
                    <span>${marca}</span>
                </div>
            </td>
            
            <td class="py-3 px-6 text-left">
                <div class="flex items-center">
                    <span>${modelo}</span>
                </div>
            </td>`;
    }

    iconDelete(){
        
        return `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
        </svg>`;
    }

    iconUpdate(){
        return `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
            </svg>
        `;
    }

    LoadedupdateProductModal(){
        this.containerUpdateProduct = document.getElementById("update-product-modal");
        this.updateModal = document.getElementById("modal-update");
        this.closeUpdateModal = document.getElementById("close-update-modal");

        this.closeUpdateModal.addEventListener("click",() => Crud.closeModal(this.containerUpdateProduct,this.closeUpdateModal));
    }
    
    LoadedDeleteProductModal(){
        this.containerDeleteProduct = document.getElementById("delete-product-modal-container");
        this.deleteModal = document.getElementById("modal-delete");
        this.closeDeleteModal = document.getElementById("close-delete-modal");

        this.closeDeleteModal.addEventListener("click",() => Crud.closeModal(this.containerDeleteProduct,this.deleteModal));
    }
    
    LoadednewProductModal(){

        this.containerNewModal = document.getElementById("modal-product");
        this.newModal = document.getElementById("modal-form");
        this.closeNewProductModal = document.getElementById("close-modal");
        this.btnNewProduct = document.getElementById("button-show-modal");
        
        this.btnNewProduct.addEventListener("click"        ,() => Crud.activeModal(this.containerNewModal,this.newModal))
        this.closeNewProductModal.addEventListener("click" ,() => Crud.closeModal(this.containerNewModal,this.closeNewProductModal));
    }
      
    async submitUpdate(e){
        e.preventDefault();
        if(!this.validateData(e.target)){
            alert("complete los campos");
            return
        }

        let fields = [...this.row.querySelectorAll("div")]

        this.row.classList.add("transition", "duration-500", "bg-green-100")
        
        setTimeout(() => {
            this.row.classList.remove("transition", "duration-500", "bg-green-100")
        }, 1000);

       
        let res = await this.fetchData("http://localhost:8000/products/update","PUT")
        
        if(res.http_code === 200){

            Toastify({
                text: "producto actualizado",
                duration: 3000,
                backgroundColor: "linear-gradient(to right, #00b09b, #96c93d)",
            }).showToast();

            this.actualproduct = res.data
            this.closeUpdateModal.click()

            fields[1].children[0].textContent = this.product["nombre"]
            fields[2].children[0].textContent = this.product["lote"]
            fields[3].children[0].textContent = this.product["vencimiento"]
            fields[4].children[0].textContent = this.product["marca"]
            fields[5].children[0].textContent = this.product["modelo"]

            this.closeUpdateModal.click()
        }else{
            alert("ocurrio un error al momento de actualizar el producto")
        }
            
   
    }

    async submitDelete(e){
        e.preventDefault();

        let res = await this.fetchData("http://localhost:8000/products/delete","DELETE",{id: this.idProduct})
        
        if(res.http_code === 200){
            Toastify({
                text: "producto eliminado",
                duration: 3000,
                backgroundColor: "linear-gradient(to right, #00b09b, #96c93d)",
            }).showToast();

            this.closeDeleteModal.click()
            
            this.row.classList.add("transition", "duration-500", "bg-red-100")
    
            setTimeout(() => this.tbody.removeChild(this.row), 1000);
        }else{
            alert("ocurrio un error al momento de registrar el producto")
        }
        
    }
    
    async submitNew(e){
        e.preventDefault();

        if(!this.validateData(e.target)){
            alert("complete los campos");
            return
        }

        let notProducts = document.getElementById("no-data");
        if(notProducts !== null) this.tbody.removeChild(notProducts);

        let res = await this.fetchData("http://localhost:8000/products/create","POST")
        
        if(res.http_code === 200){
            
            Toastify({
                text: "producto registrado",
                duration: 3000,
                backgroundColor: "linear-gradient(to right, #00b09b, #96c93d)",
            }).showToast();

            delete res.data.updated_at;
            delete res.data.created_at;
        
            this.actualproduct = res.data;
            this.products.push(this.actualproduct);
            this.closeNewProductModal.click();
            
        }else{
            alert("ocurrio un error al momento de registrar el producto")
        }
        
        this.tbody.appendChild(this.productRow(this.actualproduct));

    }

    async fetchData(url,method,id = null){
        let res = await fetch(url,{ method,
                        headers: {
                            "X-CSRF-TOKEN": CSRF_TOKEN,
                        },
                        body: JSON.stringify( id || this.product)
                    })
        return res.json()
    }

    updateData({id,nombre,lote,vencimiento,marca,modelo},tr){
        this.row = tr;
        this.idProduct = id;
        this.formUpdateProduct.nombre.value = nombre;
        this.formUpdateProduct.lote.value = lote;
        this.formUpdateProduct.vencimiento.value = vencimiento;
        this.formUpdateProduct.marca.value = marca;
        this.formUpdateProduct.modelo.value = modelo; 
    }

    validateData(form){
        for (const item of form) {
            if (item.type == "text" || item.type == "date" ) {
                this.product[item.name] = item.value
            }
        }

        for (const i in this.product){
            if(this.product[i] == null ||this.product[i].trim() == "" ){
                return false;
                break;
            }
        }
        this.product["id"] = this.idProduct.toString(); 
        return true;

    }

    deleteData({id},tr){
        this.row = tr;
        this.idProduct = id;
    }

    static activeModal(modal,modal_form){
        modal.classList.add("opacity-100","events","ease-out","duration-300")
        modal_form.classList.add("opacity-100", "translate-y-0", "sm:scale-100");
    }

    static closeModal(modal,modal_form){
        modal_form.classList.remove("opacity-100", "translate-y-0", "sm:scale-100");
        modal.classList.remove("opacity-100","events","ease-out","duration-300")
        modal.classList.add("ease-in","duration-200")
    }

}

export default Crud;