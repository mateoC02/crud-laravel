class Modal{

    constructor(){
        this.idProduct = null;
        
        this.formUpdate = document.getElementById("update-form");
        this.formDelete = document.getElementById("delete-form");
        this.formNew = document.getElementById("new-form");
        
        this.formUpdate.addEventListener("submit", (e)=> this.submitUpdate(e));
        this.formDelete.addEventListener("submit", (e)=> this.submitDelete(e));
        this.formNew.addEventListener("submit", (e)=> this.submitNew(e));
        
        this.data = {
            nombre : null,
            lote : null,
            vencimiento : null,
            marca : null,
            modelo : null
        };
    }        

    submitUpdate(e){
        e.preventDefault();
        if(!this.createData(e.target)) alert("complete los campos");

        (async()=>{
            console.log(this.data);
            // let res = await this.fetchData("http://localhost:8000/products/update","POST")
           
            // if(res.http_code === 200){
            //     alert("producto registrado")
            // }else{
            //     alert("ocurrio un error al momento de registrar el producto")
            // }
            
        })()
    }

    submitDelete(e){
        e.preventDefault();
        alert(this.idProduct)
    }
    
    submitNew(e){
        e.preventDefault();
        if(!this.createData(e.target)) alert("complete los campos");

        (async()=>{

            let res = await this.fetchData("http://localhost:8000/products/create","POST")
            
            if(res.http_code === 200){
                alert("producto registrado")
            }else{
                alert("ocurrio un error al momento de registrar el producto")
            }
            
        })()
        
    }

    async fetchData(url,method){
        let res = await fetch(url,{ method,
                        headers: {
                            "X-CSRF-TOKEN": CSRF_TOKEN,
                        },
                        body: JSON.stringify(this.data)
                    })
        return res.json()
    }

    updateData({id,nombre,lote,vencimiento,marca,modelo}){
        this.idProduct = id;
        this.formUpdate.nombre.value = nombre;
        this.formUpdate.lote.value = lote;
        this.formUpdate.vencimiento.value = vencimiento;
        this.formUpdate.marca.value = marca;
        this.formUpdate.modelo.value = modelo; 
    }

    createData(form){
        for (const item of form) {
            if (item.type == "text" || item.type == "date" ) {
                this.data[item.name] = item.value
            }
        }

        for (const i in this.data){
            if(this.data[i] == null ||this.data[i].trim() == "" ){
                return false;
                break;
            }
        }

        return true;

    }

    deleteData({id}){
        this.idProduct = id;
    }

    static activeModal(modal,modal_form){
        modal.classList.add("opacity-100","events","ease-out","duration-300")
        modal_form.classList.add("opacity-100", "translate-y-0", "sm:scale-100");
    }

    static closeModal(modal,modal_form){
        modal_form.classList.remove("opacity-100", "translate-y-0", "sm:scale-100");
        modal.classList.remove("opacity-100","events","ease-out","duration-300")
        modal.classList.add("ease-in","duration-200")
    }

}

export default Modal;