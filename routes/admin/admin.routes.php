<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AboutController;

/*
* 
* ROUTES FOR ADMIN
*
*/

return function () {
    Route::get('/',          [AboutController::class,"index"]);
    Route::get('/user/{id?}',[AboutController::class, "user"]);
};