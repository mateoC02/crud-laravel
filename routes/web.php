<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// index route
Route::get("/", fn () => view("welcome"));

Route::prefix("products")->group(function(){
    Route::get('/',       [ProductController::class,'index']);
    Route::get('/get',    [ProductController::class,'get']);
    Route::post('/create',[ProductController::class,'create']);
    Route::put('/update', [ProductController::class,'update']);
    Route::delete('/delete', [ProductController::class,'delete']);
});

// Route::prefix('admin')->group(require_once "admin/admin.routes.php");
